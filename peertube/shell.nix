{
  pkgs ? import <nixpkgs> {}
}:

with pkgs; mkShell {
  shellHook = ''
    PEERTUBE=${peertube}
    function deploy {
      rm -rf /tmp/{psql,peertube}
      mkdir -p /tmp/peertube/{config,storage}
      cp -r $PEERTUBE /tmp/peertube/code
      mkdir /tmp/psql
      initdb -D /tmp/psql/ --no-locale --encoding=UTF8
      pg_ctl -D /tmp/psql -l logfile -o "--unix_socket_directories='$PWD'" start
      createdb -h $PWD peertube
      psql -h $PWD peertube -c "CREATE EXTENSION pg_trgm;"
      psql -h $PWD peertube -c "CREATE EXTENSION unaccent;"
      cp $PEERTUBE/config/default.yaml /tmp/peertube/config/default.yaml
      cp production.yaml /tmp/peertube/config/production.yaml
      echo "you must set aws keys in the peertube's config. opening vim in 3 seconds."
      sleep 3
      vim "/tmp/peertube/config/production.yaml"
      echo "Launch redis-server"
      redis-server &
      sleep 3
      cd /tmp/peertube/code
      export NODE_ENV=production
      export NODE_CONFIG_DIR=/tmp/peertube/config
      node dist/server.js
      # npm run reset-password -- -u root # to set/change root password
      # pg_ctl -D /tmp/psql stop
      # stop redis
    }
  '';
  nativeBuildInputs = [
    peertube
    postgresql_14
    curl
    unzip
    nodejs-16_x
    yarn
    ffmpeg
    openssl
    redis
    gcc
    nginx
  ];
}
