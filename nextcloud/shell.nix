{
  pkgs ? import <nixpkgs> {}
}:

pkgs.mkShell {
  shellHook = ''
  NEXTCLOUD=${pkgs.nextcloud22}
  function deploy {
    rm -rf /tmp/nextcloud
    cp -r $NEXTCLOUD/ /tmp/nextcloud
    cd /tmp/nextcloud
    chmod +w -R .
    php -S 0.0.0.0:9000
  }
  '';
  nativeBuildInputs = [
    pkgs.nextcloud22
    pkgs.php80
  ];
}
